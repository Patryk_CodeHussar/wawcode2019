import React, { useState } from 'react';


import styled from 'styled-components'

import axios from 'axios'

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Checker from "./Checker"
import LinearProgress from '@material-ui/core/LinearProgress';

import Divider from '@material-ui/core/Divider';


export class DataInput2 extends React.Component {
    constructor() {
        super();
        this.state = {  };
        this.state={
            value:[],
            data: [],
        }
        var i=0;
    }

    findPic(name) {
        switch (name) {
            case "Bench":
                return "./resources/branch.png";
            case "Pavement":
                return "./resources/people.png";
            case "Arbor":
                return "./resources/park.png";
            case "Bus stop":
                return "./resources/road.png";
            case "Elevator":
                return "./resources/people.png";
            case "Green":
                return "./resources/tree.png";
            case "Park":
                return "./resources/park.png";
            case "Fountain":
                return "./resources/fountain.png";
            default:
                break;
        }
    }

    componentDidMount() {
        if (typeof window.sessionStorage.fav !== "undefined") {

            let arr = window.sessionStorage.fav.split(",");
            arr.forEach(element => {
                axios.get(`http://uranus.wat.edu.pl:7050/pw2/api/getProject/${element}`)
                    // .then(res => this.state.data.push(res.data))
                    .then(res => { this.setState({ data: this.state.data.concat([res.data]) }) });

            });
        }

        // fetch(`http://uranus.wat.edu.pl:7050/pw2/api/getProject/1`)
        //     .then(res => res.json())
        // .then(json => this.setState({ data: json }));
    }


    
     nazwa(element){
        switch (element.name) {
            case "Bench":
                return "Ławka";
            case "Pavement":
                return "Chodnik";
            case "Arbor":
                return "Altanka";
            case "Bus stop":
                return "Przystanek";
            case "Elevator":
                return "Winda";
            case "Green":
                return "Drzewo";
            case "Park":
                return "Park";
            case "Fountain":
                return "Fontanna";
            default:
                break;
        }
    }

    change(i){
        if(this.state.value[i]==1){
            var tmp = this.state.value;
            tmp[i]=0; 
            this.setState(
                {
                    value:tmp
                }
            )
        }else{
            var tmp = this.state.value;
            tmp[i]=1; 
            this.setState(
                {
                    value:tmp
                }
            )
        }
    }


    render() {

        return (
            <Centred>
                {this.i=0}
                <CardCentered>
                    <MyDiv>
                        <MyButton onClick={this.props.auth} size="small">
                            Powrót
                    </MyButton>
                        <List>
                            {this.state.data.map(element => (                       
                                <>
                                    <ListItem alignItems="flex-start">
                                        <ListItemAvatar>
                                            <Avatar alt="Project1" src={this.findPic(element.name)} />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={this.nazwa(element)}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography component="span" color="textPrimary">
                                                         {this.nazwa(element.name)}
                                                    </Typography>
                                                    {"Lorem Veritatis ut aut nemo nemo. Accusantium suscipit a rem quia vel magni voluptatem accusantium... "}
                                                    <div className="row">
                                                        <div className="col-8">
                                                            <h4>{element.founds} / {element.foundsMAX}PLN</h4>
                                                        </div>
                                                        <div className="col-4">
                                                            <Checker/>
                                                        </div>
                                                    </div>
                                                    <Holder style={{ flexGrow: 0 }}>
                                                        <MyLinearProgress color="primary" variant="determinate" value={(element.founds / element.foundsMAX) * 100} />
                                                    </Holder>
                                                </React.Fragment>
                                            }
                                        />


                                    </ListItem>
                                    <Divider />
                                </>
                            ))}
                        </List>
                    </MyDiv>
                </CardCentered>
            </Centred>

        );
    }
}


const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-image: url("./img/background.png"); 

    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    width: 100%;
    height:100vh;  
   


`
const CardCentered = styled.div`
display:absolute;
z-index:2
// padding-top:5vh;


`

const MyDiv = styled.div`
padding: 10px !important;
margin: 10px !important;
text-align: center;
border-radius: 10px;
background-color: #afb4c9;
display: inline-block;
`

const MyButton = styled.span`
:hover {cursor: pointer};
margin: 20px 20px 20px 20px;
padding: 10px;  
background-color: #1364AB;
color: #F7F7F7;
display: block;


`
const Holder = styled.div`
-webkit-flex-grow: 50;
flex-grow: 50;
width: 100%;
height: 1000%;
`

const MyLinearProgress = styled(LinearProgress)`
color: #FF6600
`
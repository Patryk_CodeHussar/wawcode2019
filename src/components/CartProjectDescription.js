import React, { useState } from 'react';

import TextField from '@material-ui/core/TextField';

import styled from 'styled-components'

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
});

export function CartProjectDescription(props) {
    let data = props.data

    let Uni = () => {
        return <Logo className="img-fluid" src="./resources/cyberGuru.png"></Logo>;

    }
    return <Centred>
        <CardCentered>
            <MyImage src="./resources/logo.png" alt="logo" height="" />
            <br />
            <MyDiv>
                <div className="row">
                    <div className="col-5">
                        {Uni()}
                    </div>
                    <MyTitle className="col-6">
                        <h2>Name</h2>
                        <h5>Project name</h5>
                    </MyTitle>
                    <MyList>
                        <li>
                            <h5>Data końca projektu: 10.10.10</h5>
                        </li>
                        <li>
                            <h5>Opis</h5>
                        </li>
                        <h5>araararararararararar</h5>
                    </MyList>
                </div>
            </MyDiv>
        </CardCentered>
    </Centred>
}

const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-image: linear-gradient(to bottom right, #0f0c29, #302b63);
   


`// background-color:#1C1C24FF
const CardCentered = styled.div`
display:absolute;
z-index:2
// padding-top:10vh;


`

const MyDiv = styled.div`
text-align: left;
padding: 20px;
border-radius: 10px;
background-color: #afb4c9;
display: inline-block;
`


const MyButton = styled.span`
:hover {cursor: pointer};
margin: 20px 20px 20px 20px;
padding: 10px;  
background-color: #565665;
color: #F7F7F7;
display: block;
`
const MyInput = styled(TextField)`

display: block;

`
const MyImage = styled.img`
width:98px;
height:137px;

margin-bottom: 10vh;

`


const Card = styled.div`
margin-top: 5%
margin-left:5vw;
padding: 0px 0px 0px 10px;
position: absolute;
width: 90vw;
height: 70vh;
border: 1px solid black;
background: #ccc;
padding: 20px;
fontSize: 3em;
`

const Level = styled.div`
margin-top:8vw;
margin-right: 5vw;
margin-left: 5vw;
`
const Skills = styled.div`
margin-top:10vw;
`
const Skill = styled.img`
margin:0;
`

const Star = styled.div`
`

const MyTitle = styled.div`
    margin-top:10px;
`

const Logo = styled.img`
max-height: 150px;
`

const Przycisk = styled.img`
width:100%;
margin-left:0;
margin-right:0;
`

const MyCol = styled.div`
padding: 0px !important;
`


const MyCol2 = styled.div`
padding: 5px !important;
`

const MyList = styled.ul `
list-style-type: none;
`
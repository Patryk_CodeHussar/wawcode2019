import React, { useState } from 'react';

import TextField from '@material-ui/core/TextField';

import styled from 'styled-components'

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


export function AddNewProject(props) {


    const [uniersity, setUniersity] = useState("");
    let handleChangeUniversity = event => {
        setUniersity(event.target.value);
    };

    const [department, setDepartment] = useState("");
    let handleChangeDepartment = event => {
        setDepartment(event.target.value);
    };

    const [fieldOfStudy, setFOS] = useState("");
    let handleChangeFOS = event => {
        setFOS(event.target.value);
    };

    const [description, setDescription] = useState("");
    let handleChangeDescription = event => {
        setDescription(event.target.value);
    };

    // const [description, setDescription] = useState("");
    let handleChange = event => {
        // setFOS(event.target.value);
    };


    return <Centred>
        <CardCentered>
            <br></br>
            <MyDiv>
                <MyInput
                    id="outlined-name"
                    label="Nazwa Projektu"
                    value={uniersity}
                    onChange={handleChangeUniversity}
                    margin="normal"
                    variant="outlined"
                />
                <MyInput
                    id="outlined-name"
                    label="Uniwersytet"
                    value={department}
                    onChange={handleChangeDepartment}
                    margin="normal"
                    variant="outlined"
                />
                <MyInput
                    id="outlined-name"
                    label="Kierunek"
                    value={fieldOfStudy}
                    onChange={handleChangeFOS}
                    margin="normal"
                    variant="outlined"
                />
                <br></br>
                <MyInput
                    style={{ width: "100%" }}
                    id="outlined-multiline-flexible"
                    label="Opis projektu"
                    multiline
                    rows="3"
                    value={description}
                    onChange={handleChangeDescription}
                    margin="normal"
                    variant="outlined"
                />

                <br></br>
                <FormControlLabel
                    control={
                        <Checkbox
                            // checked={this.state.checkedA}
                            onChange={handleChange('checkedA')}
                            value="checkedA"
                        />
                    }
                    label="Java"
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            // checked={this.state.checkedA}
                            onChange={handleChange('checkedA')}
                            value="checkedA"
                        />
                    }
                    label="CSS"
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            // checked={this.state.checkedA}
                            onChange={handleChange('checkedA')}
                            value="checkedA"
                        />
                    }
                    label="Android"
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            // checked={this.state.checkedA}
                            onChange={handleChange('checkedA')}
                            value="checkedA"
                        />
                    }
                    label="CPP"
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            // checked={this.state.checkedA}
                            onChange={handleChange('checkedA')}
                            value="checkedA"
                        />
                    }
                    label="C#"
                />
                <br></br>
                    
                <MyButton onClick={props.auth} size="small">
                    Utwórz projekt
                </MyButton>
            </MyDiv>
        </CardCentered>
    </Centred>
}

const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-image: linear-gradient(to bottom right, #0f0c29, #302b63);
   

`
const CardCentered = styled.div`
display:absolute;
z-index:2
// padding-top:5vh;


`

const MyDiv = styled.div`
padding: 10px !important;
margin: 10px !important;
text-align: center;
border-radius: 10px;
background-color: #afb4c9;
display: inline-block;
`


const MyButton = styled.span`
:hover {cursor: pointer};
margin: 20px 20px 20px 20px;
padding: 10px;  
background-color: #565665;
color: #F7F7F7;
display: block;
`
const MyInput = styled(TextField)`
padding: 5px !important;
margin: 5px !important;
display: block;

`
// ./resources/
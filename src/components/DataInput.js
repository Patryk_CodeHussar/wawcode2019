import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import styled from 'styled-components'
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';



export function DataInput(props) {


    const [uniersity, setUniersity] = useState("");
    let handleChangeUniversity = event => {
        setUniersity(event.target.value);
    };

    const [department, setDepartment] = useState("");
    let handleChangeDepartment = event => {
        setDepartment(event.target.value);
    };

    const [fieldOfStudy, setFOS] = useState("");
    let handleChangeFOS = event => {
        setFOS(event.target.value);
    };

    const [description, setDescription] = useState("");
    let handleChangeDescription = event => {
        setDescription(event.target.value);
    };

    // const [description, setDescription] = useState("");
    let handleChange = event => {
        // setFOS(event.target.value);
    };


    return <Centred>
        <CardCentered>
            <br></br>
            <MyDiv>
                <h2>What projects would u like to support?</h2>
                <br></br>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                // checked={this.state.checkedA}
                                onChange={handleChange('checkedA')}
                                value="checkedA"
                            />
                        }
                        label="Culture"
                    />
                </FormGroup>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                // checked={this.state.checkedA}
                                onChange={handleChange('checkedA')}
                                value="checkedA"
                            />
                        }
                        label="Environment"
                    />
                </FormGroup>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                // checked={this.state.checkedA}
                                onChange={handleChange('checkedA')}
                                value="checkedA"
                            />
                        }
                        label="Green"
                    />
                </FormGroup>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                // checked={this.state.checkedA}
                                onChange={handleChange('checkedA')}
                                value="checkedA"
                            />
                        }
                        label="Public space"
                    />
                </FormGroup>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                // checked={this.state.checkedA}
                                onChange={handleChange('checkedA')}
                                value="checkedA"
                            />
                        }
                        label="Sport"
                    />
                </FormGroup>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                // checked={this.state.checkedA}
                                onChange={handleChange('checkedA')}
                                value="checkedA"
                            />
                        }
                        label="Trafic"
                    />
                </FormGroup>

                <MyButton onClick={props.auth} size="small">
                    Confirm 
                </MyButton>
            </MyDiv>
        </CardCentered>
    </Centred >
}

const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-image: linear-gradient(to bottom right, #0f0c29, #302b63);
   


`
const CardCentered = styled.div`
display:absolute;
z-index:2
// padding-top:5vh;


`

const MyDiv = styled.div`
padding: 10px !important;
margin: 10px !important;
text-align: center;
border-radius: 10px;
background-color: #afb4c9;
display: inline-block;
`


const MyButton = styled.span`
:hover {cursor: pointer};
margin: 20px 20px 20px 20px;
padding: 10px;  
background-color: #1364AB;
color: #F7F7F7;
display: block;
`
const MyInput = styled(TextField)`
padding: 5px !important;
margin: 5px !important;
display: block;

`
// ./resources/
import React, { useState } from 'react';

import TextField from '@material-ui/core/TextField';

import styled from 'styled-components'



export function Login(props) {

    // const { classes } = this.props;

    const [name, ] = useState("demo1");
    const [password, ] = useState("demo1");
    

    let handleChange = event => {
        // setName(event.target.value);
    };

  

    return <Centred>
        <CardCentered>
            <MyImage src= "./resources/logo.png" alt="logo" height = ""/>
            <br></br>
            <MyDiv>
                <MyInput
                    id="outlined-name"
                    label="Name"
                    value={name}
                    onChange={handleChange('name')}
                    margin="normal"
                    variant="outlined"
                />
                <br></br>
                <MyInput
                    id="outlined-password-input"
                    label="Password"
                    type="password"
                    value={password}
                    onChange={handleChange('password')}
                    autoComplete="current-password"
                    margin="normal"
                    variant="outlined"
                />
                <br></br>
                <MyButton onClick={props.auth} size="small">
                    Log in
                        </MyButton>
            </MyDiv>
        </CardCentered>
    </Centred>
}

const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-image: "src/img/background.png"
   


`// background-color:#1C1C24FF
const CardCentered = styled.div`
display:absolute;
z-index:2
padding-top:10vh;


`

const MyDiv = styled.div`
text-align: center;
padding: 20px;
border-radius: 10px;
background-color: #afb4c9;
display: inline-block;
`


const MyButton = styled.span`
:hover {cursor: pointer};
margin: 20px 20px 20px 20px;
padding: 10px;  
background-color: #1364AB;
color: #F7F7F7;
display: block;
`
const MyInput = styled(TextField)`

display: block;

`
const MyImage = styled.img`
width:98px;
height:137px;

margin-bottom: 10vh;

`


// ./resources/ 
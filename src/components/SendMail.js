import TextField from '@material-ui/core/TextField';
import React, { useState } from 'react';
import styled from 'styled-components';
import axios from 'axios'

export function SendMail(props) {


    const [uniersity, setUniersity] = useState("5 PLN");
    let handleChangeUniversity = event => {
        setUniersity(event.target.value);
    };



    const clickedSendMoney = () => {
        console.log(window.sessionStorage.active)
        let fixedNumber = Number(window.sessionStorage.active)
        let fixeduniersity = Number(uniersity.replace("$", ""));
        console.log(fixedNumber, fixeduniersity);
        axios.post(`http://uranus.wat.edu.pl:7050/pw2/api/pay`, JSON.stringify({ number: fixedNumber, amount: fixeduniersity }),
            {
                headers: {
                    "Content-Type": "application/json"
                }
            })

        props.hndlModal()
    }


    return <Centred>
        <CardCentered>
            <MyDiv>
                <MyDiver>
                    <MyInput
                        id="outlined-name"
                        label="Kwota"
                        value={uniersity}
                        onChange={handleChangeUniversity}
                        margin="normal"
                        variant="outlined"
                    />
                    <br></br>
                    <MyButton onClick={clickedSendMoney} size="small">
                        Dotuj
                </MyButton>
                </MyDiver>
            </MyDiv>
        </CardCentered>
    </Centred>
}

const Centred = styled.div`
    margin: auto;    
    // margin-top: 50%
    position: absolute;
    z-index:5;
    background-image: url("./img/background.png"); 

    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    width: 100%;
    height:100vh;  
   

`
const CardCentered = styled.div`
display:absolute;
z-index:2
// padding-top:10vh;
margin: auto;
margin-top:50%;

`
const MyDiv = styled.div`
width: 80%;
height: 200px;
padding: 10px !important;
margin: auto !important;
text-align: center;
border-radius: 10px;
background-color: #ffffff;
display: block;
overflow: scroll;
`


const MyButton = styled.span`
:hover {cursor: pointer};
margin: 20px 20px 20px 20px;
padding: 10px;  
background-color: #1364AB;
color: #F7F7F7;
display: block;

`
const MyInput = styled(TextField)`
width: 100%; 
padding: 5px !important;
margin: 5px !important;
display: block;

`

const MyDiver = styled.div`
margin: auto;
`
// ./resources/



import React, { useState } from 'react';
import Swipe from 'react-easy-swipe';
import { Redirect } from 'react-router';

import styled from 'styled-components'
import { Button } from '@material-ui/core';
import { SendMail } from '../SendMail';
import { store } from '../..';


export function CardWithTechnology(props) {

    const boxStyle = {
       // left: posX,
    };

    return (
        <Card style={boxStyle}>
                <div className="row">
                    <div className="col-5">
                        <Logo className="img-fluid" src="./resources/wat.png"></Logo>
                    </div>
                    <div className="col-6">
                        <h2>Projekt</h2>
                        <h4>KZC WAT</h4>
                    </div>
                </div>
                <Skills>
                <div className="row">
                    
                        <MyCol className="col-2">
                            <Skill className="img-fluid" src="./resources/css_grey.png"></Skill>
                        </MyCol>
                        <MyCol className="col-2">
                            <Skill className="img-fluid" src="./resources/js_grey.png"></Skill>
                        </MyCol>
                        <MyCol className="col-2">
                            <Skill className="img-fluid" src="./resources/java_grey.png"></Skill>
                        </MyCol>
                        <MyCol className="col-2">
                            <Skill className="img-fluid" src="./resources/csharp_grey.png"></Skill>
                        </MyCol>
                        <MyCol className="col-2">
                            <Skill className="img-fluid" src="./resources/as_grey.png"></Skill>
                        </MyCol>
                        <MyCol className="col-2">
                            <Skill className="img-fluid" src="./resources/html_grey.png"></Skill>
                        </MyCol>
                </div>
              </Skills>
              <Level>
                <div className="row">
                    <div className="Stars">
                        <div className="row">
                            <div className="col-2">
                                <Star>
                                <img className="img-fluid" src="./resources/star.png"></img>
                                </Star>
                            </div>
                            <div className="col-2">
                                <Star>
                                <img className="img-fluid" src="./resources/star.png"></img>
                                </Star>
                            </div>
                            <div className="col-2">
                                <Star>
                                <img className="img-fluid" src="./resources/star.png"></img>
                                </Star>
                            </div>
                            <div className="col-2">
                                <Star>
                                <img className="img-fluid" src="./resources/star.png"></img>
                                </Star>
                            </div>
                            <div className="col-2">
                                <Star>
                                <img className="img-fluid" src="./resources/starempty.png"></img>
                                </Star>
                            </div>
                            <div className="col-2">
                                <Star>
                                <img className="img-fluid" src="./resources/starempty.png"></img>
                                </Star>
                            </div>
                        </div>
                    </div>
                </div>
              </Level>
            </Card>
    )
}

const Card = styled.div`
margin-top: 20%
margin-left:5vw;
padding: 0px 0px 0px 10px;
position: absolute;
width: 90vw;
height: 60vh;
border: 1px solid black;
background: #ccc;
padding: 20px;
fontSize: 3em;
`

const Level=styled.div`
margin-top:8vw;
margin-right: 5vw;
margin-left: 5vw;
`
const Skills=styled.div`
margin-top:10vw;
`
const Skill = styled.img`
margin:0;
`

const Star=styled.div`
`

const Logo = styled.img`
max-height: 150px;
`

const Przycisk = styled.img`
width:100%;
margin-left:0;
margin-right:0;
`

const MyCol = styled.div`

padding: 0px !important;
`
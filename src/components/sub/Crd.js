import React, { useState } from 'react';
import Swipe from 'react-easy-swipe';
import Progress from 'react-progressbar';
import {Container, Row} from 'react-bootstrap'
import Col from 'react-bootstrap/Container'
import styled from 'styled-components'
import { SendMail } from '../SendMail';
import LinearProgress from '@material-ui/core/LinearProgress';



export function Crd(props) {

    const [posX, setPosX] = useState();
    const [cardStatus, setCardStatus] = useState(0);



    const onSwipeMove = (position, event) => {
        // console.log("psoit", position.x);
        // console.log(posX)
        if (posX > -500 && posX < 500 && position.x < posX) setPosX(position.x)
        setPosX(position.x)

        // console.log(`Moved ${position.y} pixels vertically`, event);
        // if (posX > -500 && posX < 500) setPosX(position.y)
        // setPosY(position.y)

    }

    const onSwipeEnd = (event) => {
        // console.log('End swiping...', event);
        if (posX < -100) {
            // setCardStatus(2)
            props.handleSw(props.number, true)

        }
        if (posX > 100) {
            props.handleSw(props.number, true)
            props.hndlModal()
            setCardStatus(20)
            // go to my view here
        }

        setPosX(0)

    }

    console.log("Props.number = ", )
    if (cardStatus > 1) {
        
        return <SendMail userId={props.number}>  </SendMail>

    }
    return (
        <Swipe
            // onSwipeStart={onSwipeStart}
            onSwipeMove={onSwipeMove}
            onSwipeEnd={onSwipeEnd}>
            <MyProjecCard pos={posX} data={props.data} />
        </Swipe>
    );
}


function MyProjecCard(props) {
    const boxStyle = {
        left: props.pos,
    };

    let data = props.data
    if (!data) return ''


    let Uni = () => {
        switch (data.name) {
            case "Park":
                return <Logo className="img-fluid" src="./resources/park.png"></Logo>;
            case "Bench":
                return <Logo className="img-fluid" src="./resources/branch.png"></Logo>;
            case "Pavement":
                return <Logo className="img-fluid" src="./resources/people.png"></Logo>;
            case "Arbor":
                return <Logo className="img-fluid" src="./resources/arbor.png"></Logo>;
            case "Bus stop":
                return <Logo className="img-fluid" src="./resources/road.png"></Logo>;
            case "Elevator":
                return <Logo className="img-fluid" src="./resources/people.png"></Logo>;
            case "Green":
                return <Logo className="img-fluid" src="./resources/tree.png"></Logo>;
            case "Park":
                return <Logo className="img-fluid" src="./resources/park.png"></Logo>;
            case "Fountain":
                return <Logo className="img-fluid" src="./resources/fountain.png"></Logo>;
            default:
                break;
        }
    }

    let Stars = () => {
        let arr = [1]
        for (let i = 0; i < data.level; i++) {
            arr.push(1)
        }
        while (arr.length < 6) {
            arr.push(0);
        }
        let i = 0
        return (
            <Holder style={{ flexGrow: 0 }}>
                <MyLinearProgress color="primary" variant="determinate" value={(data.founds / data.foundsMAX) * 100} />
            </Holder>
        )
    }
    
    const nazwa=()=>{
        switch (data.name) {
            case "Bench":
                return "Ławka";
            case "Pavement":
                return "Chodnik";
            case "Arbor":
                return "Altanka";
            case "Bus stop":
                return "Przystanek";
            case "Elevator":
                return "Winda";
            case "Green":
                return "Drzewo";
            case "Park":
                return "Park";
            case "Fountain":
                return "Fontanna";
            default:
                break;
        }
    }

    return <Card onClick={() => { }} style={boxStyle}>

            <Container>
                <Col></Col>
                <Col>
                    <ImageView>
                         {Uni()}
                    </ImageView>
                </Col>      
                <Col></Col>
            </Container>
            <Container>
                <Col></Col>
                <Col>
                    <HeaderProject>
                         <h3>{nazwa()}</h3>
                    </HeaderProject>
                </Col>
                <Col></Col>
            </Container>

            <Container>
                <Col></Col>
                <Col>
                    <Container>
                            <HeaderProject2>
                                <h5>{data.projectName}</h5>              
                            </HeaderProject2>
                    </Container>
                </Col>
                <Col>

                </Col>
            </Container>

        <Description>
          <h6>{data.description.toString().slice(0, 80)}</h6>
        </Description>
        
        <Skills>
            <div className="row">
                <MyCol className="col-2">
                    {!(data.technology[0]) &&
                        <Skill className="img-fluid" src="./resources/categories/first.png" height="42" width="42"></Skill>
                    }
                    {!!(data.technology[0]) &&
                        <Skill className="img-fluid" src="./resources/categories/first_grey.png" height="42" width="42"></Skill>
                    }
                </MyCol>
                <MyCol className="col-2">
                    {!(data.technology[0]) &&
                        <Skill className="img-fluid" src="./resources/categories/second.png" height="42" width="42"></Skill>
                    }
                    {!!(data.technology[0]) &&
                        <Skill className="img-fluid" src="./resources/categories/second_grey.png" height="42" width="42"></Skill>
                    }
                </MyCol>
                <MyCol className="col-2">
                    {!(data.technology[1]) &&
                        <Skill className="img-fluid" src="./resources/categories/third.png" height="42" width="42"></Skill>
                    }
                    {!!(data.technology[1]) &&
                        <Skill className="img-fluid" src="./resources/categories/third_grey.png" height="42" width="42"></Skill>
                    }
                </MyCol>
                <MyCol className="col-2">
                    {!(data.technology[2]) &&

                        <Skill className="img-fluid" src="./resources/categories/four.png" height="42" width="42"></Skill>}
                    {!!(data.technology[2]) &&

                        <Skill className="img-fluid" src="./resources/categories/four_grey.png" height="42" width="42"></Skill>}
                </MyCol>
                <MyCol className="col-2">
                    {!(data.technology[3]) &&

                        <Skill className="img-fluid" src="./resources/categories/five.png" height="42" width="42"></Skill>}
                    {!!(data.technology[3]) &&

                        <Skill className="img-fluid" src="./resources/categories/five_grey.png" height="42" width="42"></Skill>}
                </MyCol>
                <MyCol className="col-2">
                    {!(data.technology[4]) &&

                        <Skill className="img-fluid" src="./resources/categories/six.png" height="42" width="42"></Skill>}
                    {!!(data.technology[4]) &&

                        <Skill className="img-fluid" src="./resources/categories/six_grey.png" height="42" width="42"></Skill>}
                </MyCol>
            </div>
        </Skills>
        <Level>
            <div className="row">
                <div className="col-8">
                    <Price>
                        <h5>{data.founds} / {data.foundsMAX}PLN</h5>
                    </Price>
                </div>
                <div className="col-4">
                    <ImageView2>
                        <FavStar id={data.id} /> 
                    </ImageView2>
                </div>
            </div>
        </Level>
    </Card >
}



const ProgressBar = (props) => {
    return (
        <div className="progress-bar">
            <Filler percentage={props.percentage} />
        </div>
    )
}

const Filler = (props) => {
    return <div className="filler" style={{ width: `${props.percentage}%` }} />
}

const Price=styled.div`
margin-top:15px;
`

const ImageView=styled.div`
margin-left: auto;
margin-right: auto;
display: block;
`

const Card = styled.div`
                margin-top: 5%
                margin-left:5vw;
                padding: 0px 0px 0px 10px;
                position: absolute;
                box-shadow: 1px 3px 31px -13px rgba(0,0,0,0.75);
                width: 90vw;
                height: 65vh;
                background: #ffff;
                padding: 20px;
                fontSize: 3em;
                `

const Level = styled.div`
                margin-top:8vw;
                margin-right: 5vw;
                margin-left: 5vw;
                `
const Skills = styled.div`
                margin-top:10vw;
                `
// const Skills2 = styled.div`
// margin-top:5vw;
// padding-bottom: 5px;
//                 `
const Skill = styled.img`
                margin:0;
                `

const Center = styled.div`
text-align: center;
                `

const MyTitle = styled.div`
                    margin-top:10px;
                `
const ImageView2=styled.div`
padding: 5%;
`

const Logo = styled.img`
                max-height: 150px;
                `

// const Przycisk = styled.img`
//                 width:100%;
//                 margin-left:0;
//                 margin-right:0;
//                 `

const MyCol = styled.div`
                padding: 0px !important;
                `


const MyCol2 = styled.div`
                padding: 5px !important;
                `
const HeaderProject=styled.div`
text-align:center;
`
// const MyInput = styled(TextField)`
// * {
//                     font - size: 20px !important;
//                 color: #000000 !important;
//             } 
//             display: block;

// `


const Holder = styled.div`
-webkit-flex-grow: 50;
flex-grow: 50;
width: 100%;
height: 1000%;
`
const Description=styled.div`
text-align:center;
`

const MyLinearProgress = styled(LinearProgress)`
color: #FF6600
`
const FavStar = (props) => {

    const [isIn, setcolor] = useState(0)



    const handleColr = () => {
        if (isIn > 0) {
            setcolor(0)
        }
        else {
            setcolor(1)
        }

    }

    // console.log("Is in: " + isIn);

    const addToFavorite = (id) => {
        let tmp = []
        if (typeof window.sessionStorage.fav !== "undefined") {
            tmp = window.sessionStorage.fav.split(',')
        }
        console.log(tmp)
        let a = id.toString()
        console.log(a)
        console.log(!tmp.includes(a))
        if (!tmp.includes(a)) {
            tmp.push(id.toString());
        }
        
        window.sessionStorage.fav = tmp;
        console.log(tmp, "\n")
        handleColr()
    }

    if (isIn > 0) {
        return (
            <div>
                <MyImg src="./resources/serce.png" onClick={() => { addToFavorite(props.id) }} />
            </div>
        )
    } else {
        return (
            <div>
                <MyImg src="./resources/heart2.png" onClick={() => { addToFavorite(props.id) }} />
            </div>
        )
    }
}
const MyImg = styled.img`
height:50px;
`

const FavMovingDiv = styled.div`
position:absolute;
margin-top:-30px;
margin-left:230px;
z-index:3
`

const HeaderProject2=styled.div`
color:grey;
text-align:center;
`

import React, { useState } from 'react';
import axios from 'axios'
import styled from 'styled-components'

import { Link } from 'react-router-dom'

// import { store } from '../../index'
import { Crd } from './Crd';
import { SendMail } from '../SendMail'
import { DataInput2 } from '../DataInput2'
import { DataInput } from '../DataInput'

let once = true;

export function PrjCard(props) {

    const [card1, visCard1] = useState(true)
    const [card2, visCard2] = useState(true)

    const [prj1, setPrj1] = useState()
    const [prj2, setPrj2] = useState()
    const [second, setSecond] = useState()

    const [temp, setTemp] = useState()




    let type = props.type;
    let url = () => {
        if (type === "usr") {
            return "http://uranus.wat.edu.pl:7050/pw2/api/getUser"
        }
        else {
            return "http://uranus.wat.edu.pl:7050/pw2/api/getNext"

        }

    }






    const handleSwipe = (num, like) => {
        // console.log("FIREEEED", num)
        switch (num) {
            case 1:
                visCard1(false)
                visCard2(true)
                setPoints(points + 1)
                axios.get(url())
                    .then(res => {
                        window.sessionStorage.active = res.data.id;
                        setPrj1(res.data)
                        setSecond(res.data)
                    })

                break;
            case 2:
                visCard2(false)
                visCard1(true)
                axios.get(url())
                    .then(res => {
                        window.sessionStorage.active = res.data.id;
                        setPrj2(res.data)
                        setSecond(res.data)
                    })

                break;
            default:
                break;
        }
        // let temp = store.getState().cardRedu;
        setTemp(temp + 1)

        // store.dispatch({ type: "NUMBER", payload: num })
        // console.log(store.getState().cardRedu)
        // // console.log(numbers)
    }

    const componentDidMount = () => {

        axios.get(url())
            .then(res => {
                setPrj1(res.data)
            })
        axios.get(url())
            .then(res => {
                setPrj2(res.data)
            })
        handleCard()

    }
    const handleCard = () => {
        if (card1) { handleSwipe(1); return "" };
        if (card2) { handleSwipe(2); return "" };
    }



    const handleModal = () => {
        setModal(!modal);
    }


    const handleModal2 = () => {
        // document.getElementById("reallyLink").onclick.apply(document.getElementById("reallyLink"))

        // setModal2(!modal2);
    }

    const handleModal3 = () => {
        setModal3(!modal3);
    }


    const [modal2, setModal2] = useState(false)
    const [modal, setModal] = useState(false)
    const [modal3, setModal3] = useState(false)

    const [points, setPoints] = useState(0)


    if (modal3) return <Temp> <DataInput2 auth={handleModal3} /> </Temp>
    if (modal2) return <Temp> <DataInput auth={handleModal2} /> </Temp>
    if (modal) return <>
        <SendMail

            hndlModal={handleModal}
        /> </>
    // if (dateIn) return <Upper> <DataInput hndlModal={swap} /> </Upper>
    if (once) {
        componentDidMount();
        once = false
    }
    return (<>
        {/* <UsersButton togle={() => { setdateIn(!dateIn) }} /> */}

        <div className="container">
            <MenuButton>
                <div className="row">
                    <div className="col">
                        <DescText>Mapa</DescText>
                        <Linked to="MapView" />
                        {/* <MyImg className="img-fluid" src="./resources/profile.png" onClick={handleModal2} alt="" /> */}
                    </div>
                    <div className="col-1"></div>
                    <Menu2 className="col">
                        <img alt="logo" className="img-fluid" src="./resources/logo.png"></img>
                    </Menu2>
                        {/* <Meh >Points earned: {points}</Meh> */}
                    <div className="col-1"></div>
                    <div className="col">
                        <DescText>Ulubione</DescText>
                        <MyImg2 className="img-fluid" src="./resources/serce2.png" onClick={handleModal3} alt="" />
                    </div>
                </div>
            </MenuButton>
        </div>

        {<Crd auth={props.auth} handleSw={handleSwipe} number={2} data={second}  > </Crd>}
        {card1 && <Crd auth={props.auth} hndlModal={handleModal} handleSw={handleSwipe} number={1} data={prj1} />}
        {card2 && <Crd auth={props.auth} hndlModal={handleModal} handleSw={handleSwipe} number={2} data={prj2} />}
        <DubleButoons clickGood={() => { handleModal(); handleCard() }} clickBad={handleCard} />
    </>)
}

const Meh = styled.span`
position:absolute;
color: white ;
margin-left:85px;
marign-top: 400px !importnat;
`

const Linked = styled(Link)`
margin-top:15px;
background-image: url("./resources/mess.png");
height:55px;
width:55px;
position:absolute;
background-size:     cover;                      
background-repeat:   no-repeat;
background-position: center center;              
`



const MenuButton = styled.div`
  margin-top:30px;
  margin-bottom:0;
  margin-left:10%;
  margin-right:10%;
`


function DubleButoons(props) {



    return <>
        <MyDiv>
            <Accept >
                <img onClick={props.clickGood} className="img-fluid" src="./resources/yes.png" alt=""></img>
            </Accept>
            <Decline>
                <img onClick={props.clickBad} className="img-fluid" src="./resources/no.png" alt=""></img>
            </Decline>
        </MyDiv>
    </>

}

const MyDiv = styled.div`
margin-top: 30px;
}
`

const Accept = styled.div`
position:absolute;
width:75px;
height:75px;
border-radius:50%;
margin-top: 65vh;
margin-left: 70vw
`

const Decline = styled.div`
position:absolute;
width:75px;
height:75px;
border-radius:50%;
margin-top: 65vh;
margin-left: 10vw
`

const Temp = styled.div`

`


const MyImg = styled.img`
margin-top:30%;


`
const MyImg2 = styled.img`
margin-top:30%;


`

const Menu2 = styled.div`
padding:0;
`

const DescText = styled.div`
color: #fafafa;
margin-top: -10px;
margin-left: 10px;
position: absolute;
`
import React from 'react';
import { Icon } from 'semantic-ui-react';

import './Header.css';

const header = (props) => {

    return (
        <section className='header'>
            {(window.innerWidth < 800) ? <Icon bordered color='white' name='bars' size='big' onClick={props.toggleSidebar} /> : null}
            <span> </span>
            {/* <Icon bordered color='white' name='bars' size='big' onClick={props.toggleSidebar}/> */}
            <h1>{props.title} <Icon disabled name='users' />Warsaw</h1>
            <span></span>
        </section>
    );
};

export default header;
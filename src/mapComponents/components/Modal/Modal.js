import React, {Component} from 'react';
import { Button, Checkbox, Form } from 'semantic-ui-react';
import axios from 'axios';

import './Modal.css';

class Modal extends Component {

    state={
        userName: "",
        ttl: 15,
    }

    handleUserNameChange = (event) => {
        this.setState({userName: event.target.value});
    }

    handleTTLChange = (event) => {
        this.setState({ttl: event.target.value});
    }

    createAccount = (userData) => {
        console.log('Create account')
        axios.get('http://uranus.wat.edu.pl:7050/test/api/getID')
        .then((response) => {
            console.log(response)
            const id = response.data;

            this.props.createUser(id)
            
            const newUser = { key: id, position: [52.257, 21.07], userName: this.state.userName, target: 'Waiting for the plans', icon: '' };
            return newUser;
        }).then(user => {
            console.log(user);
            axios.post('http://uranus.wat.edu.pl:7050/test/api/putUser', user);
        });
    }
    render(){

        const sectionStyle = {
            display: this.props.visibility ? "block" : "none"
        }
    

    return (
        <section className='modal' style={sectionStyle}>
            <div className='modal-window'>
                <div className='modal-window-header'>
                    <span></span>
                    <h3>CloudMeets Warsaw <br /> Logowanie</h3>
                    <span onClick={this.props.toggleModal}>X</span>
                </div>

                <Form>
                    <Form.Field>
                        <label>Nazwa Użytkownika</label>
                        <input placeholder='Nazwa' value={this.state.userName} onChange={this.handleUserNameChange}/>
                    </Form.Field>
                    <Form.Field>
                        <label>Czas widoczności(minuty)</label>
                        <input placeholder='Ilość minut' value={this.state.ttl} onChange={this.handleTTLChange}/>
                    </Form.Field>
                    <Form.Field>
                        <Checkbox label='Zgadzam się na regulamin' />
                    </Form.Field>
                    <Button type='submit' onClick={this.createAccount}>Zaloguj</Button>
                </Form>

            </div>
        </section>
    );
    }
}


export default Modal;
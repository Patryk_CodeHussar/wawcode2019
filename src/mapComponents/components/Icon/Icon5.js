import L from 'leaflet';
import svgIcon from './Icons/road.png';

const icon = new L.Icon({
    iconUrl: svgIcon,
    iconRetinaUrl: svgIcon,
    iconAnchor: null,
    popupAnchor: [-0, -0],
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: new L.Point(28, 35),
    className: 'ic'
});


export default icon;
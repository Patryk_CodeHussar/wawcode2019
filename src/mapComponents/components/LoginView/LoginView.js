import React from 'react';
import Modal from '../Modal/Modal';

import './LoginView.css';

const loginView = (props) => {

    const sectionStyle = {
        display: props.visibility ? "block" : "none"
    }

    return (
        <section className='login-view' style={sectionStyle}>
            <Modal visibility={true} createUser={props.createUser}/>
        </section>
    );
}


export default loginView;
import TextField from '@material-ui/core/TextField';
import React, { useState } from 'react';
import axios from 'axios'
import styled from 'styled-components';

export class SendMailCustom extends React.Component {

    constructor() {
        super();
        this.state = { data: {} };
    }

    componentDidMount() {
        // console.log(this.props)
        axios.get(`http://uranus.wat.edu.pl:7050/pw2/api/getProject/${this.props.targetID}`)
            // .then(res => this.state.data.push(res.data))
            .then(res => {
                this.setState({ data: res.data })
            })

        // fetch(`http://uranus.wat.edu.pl:7050/pw2/api/getProject/1`)
        //     .then(res => res.json())
        // .then(json => this.setState({ data: json }));
    }


   nazwa(name){
        switch (name) {
            case "Bench":
                return "Ławka";
            case "Pavement":
                return "Chodnik";
            case "Arbor":
                return "Altanka";
            case "Bus stop":
                return "Przystanek";
            case "Elevator":
                return "Winda";
            case "Green":
                return "Drzewo";
            case "Park":
                return "Park";
            case "Fountain":
                return "Fontanna";
            default:
                break;
        }
    }


    render() {

        // return (
        //     <Centred>
        //         <CardCentered>
        //             <MyDiv>
        //                 <MyButton onClick={this.props.auth} size="small">
        //                     Go Back
        //             </MyButton>
        //                 {/* No projects liked yet :c<br /> */}
        //                 <List>
        //                     {this.state.data.map(element => (


        return <Centred>
            < CardCentered >
                <MyDiv>
                    {/* <MyDiver> */}
                        {this.nazwa(this.state.data.name) ?
                            <>
                                <h4>{this.nazwa(this.state.data.name)}</h4>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                Duis aute irure dolor in reprehenderit
                             {/* {console.log("THERE IS DATA")} */}
                            </> : null}
                        <MyButton onClick={this.props.hndlModal} size="small">
                            Powrót do mapy
                </MyButton>


                </MyDiv>
            </CardCentered >
        </Centred >
    }
}

const Centred = styled.div`
    margin: auto;    
    // margin-top: 5%
    position: absolute;
    z-index:5;
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-image: linear-gradient(to bottom right, #0f0c29, #302b63);
   

`
const CardCentered = styled.div`
display:absolute;
z-index:2
// padding-top:10vh;
margin: auto;
margin-top:50%;

`
const MyDiv = styled.div`
width: 80%;
height: 60%;
padding: 10px !important;
margin: auto !important;
text-align: center;
border-radius: 10px;
background-color: #afb4c9;
display: block;
overflow: scroll;
`


const MyButton = styled.span`
:hover {cursor: pointer};
margin: 20px 20px 20px 20px;
padding: 10px;  
background-color: #1364AB;
color: #F7F7F7;
display: block;

`
const MyInput = styled(TextField)`
width: 100%; 
padding: 5px !important;
margin: 5px !important;
display: block;

`

const MyDiver = styled.div`
margin: auto;
`
// ./resources/
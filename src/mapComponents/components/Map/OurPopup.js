import React, { Component } from 'react';
import { Button } from '@material-ui/core'
import { SendMail } from '../../../components/SendMail'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';

function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
};




export function OurPopup({ userName, target }) {
    const [open, setOpen] = React.useState(false);
    // getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = React.useState(getModalStyle);
    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    


    console.log(userName, target);
    return (
        <div>
            <Typography gutterBottom>Click to get the full Modal experience!</Typography>
            <Button onClick={handleOpen}>Open Modal</Button>
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={handleClose}
            >
                <div style="
                    position: 'absolute',
                    width: 400,
                    backgroundColor: theme.palette.background.paper,
                    boxShadow: theme.shadows[5],
                    padding: theme.spacing(4),
                    outline: 'none' "
                >
                    <Typography variant="h6" id="modal-title">
                    Text in a modal
          </Typography>
                <Typography variant="subtitle1" id="simple-modal-description">
                    Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
          </Typography>
                <OurPopup />
                </div>
            </Modal>
        </div >
    )

}

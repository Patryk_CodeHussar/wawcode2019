import React, { Component } from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
// import { Button } from 'semantic-ui-react'
import Button from '@material-ui/core/Button';

import axios from 'axios';

import styled from 'styled-components'


import Icon1 from '../Icon/Icon1';
import Icon2 from '../Icon/Icon2';
import Icon3 from '../Icon/Icon3';
import Icon4 from '../Icon/Icon4';
import Icon5 from '../Icon/Icon5';
import Icon6 from '../Icon/Icon6';
import Icon7 from '../Icon/Icon7';
import Icon8 from '../Icon/Icon8';
import Icon9 from '../Icon/Icon9';

import UserIcon from '../Icon/UserIcon';

import { OurPopup } from './OurPopup';

import './Map.css';

export default class MyMap extends Component {
  state = {
    lat: 52.247071,
    lng: 21.062959,
    zoom: 13,
    usersTable: [
      // { key: 'marker1', position: [52.247, 21.06], userName: 'Wiktor Woźniak', target: 'Going to the cinema', icon: Icon1 }
    ],
    position: [52.260, 21.1012]
  }

  userid = this.props.userid

  serverConnection = () => {

    setTimeout(function () {

      var self = this;

      // axios.get('http://uranus.wat.edu.pl:7050/test/api/getUsers', { //http://192.168.137.245:8080/api/getUsers

      // })
      //   .then(function (response) {
      //     // console.log(response.data);
      //     self.setState({ usersTable: response.data })
      //     // console.log('State users')
      //     // console.log(self.state.usersTable)
      //   })
      //   .catch(function (error) {
      //     console.log(error);
      //   })


      this.serverConnection()
    }.bind(this), 10 * 1000);

  }
  checkGeolocation() {
    setTimeout(function () {
      let flag = true;
      var position = []
      var self = this;

      if (window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition((e) => {
          // console.log(e);
          let position = []
          position.push(e.coords.latitude)
          position.push(e.coords.longitude)
          self.setState(self.state.position = position)
        });
      } else {
        flag = false
        if (flag) {
          alert("Geolocation is not supported by this browser.");
        }
      }


      const config = {
        headers: {
          'Content-Type': 'application/json',
        }
      };
      let pos = self.state.position;
      let data = `{"position":[${pos[0]},${pos[1]}]}`
      // console.log(this.userid);



      this.checkGeolocation()
    }.bind(this), 10 * 1000);
  }

  _sendMessageHandler = (id) => {
    axios.post('');
  };
  get sendMessageHandler() {
    return this._sendMessageHandler;
  }
  set sendMessageHandler(value) {
    this._sendMessageHandler = value;
  }

  componentDidMount = () => {
    this.serverConnection();
    this.checkGeolocation();
    var self = this;

    // axios.get('http://uranus.wat.edu.pl:7050/test/api/getUsers', { //http://192.168.137.245:8080/api/getUsers

    // })
    //   .then(function (response) {
    //     // console.log(response.data);
    //     self.setState({ usersTable: response.data })
    //     // console.log('State users')
    //     // console.log(self.state.usersTable)
    //   })
    //   .catch(function (error) {
    //     console.log(error);
    //   })
  }


  
  



  render() {
    const position = [this.state.lat, this.state.lng]

    const MyMarkersList = ({ markers }) => {
      const items = markers.map(({ key, position, userName, target, icon }) => {
        let iconType;

        switch (icon) {
          case 'icon1':
            iconType = Icon1;
            break;
          case 'icon2':
            iconType = Icon2;
            break;
          case 'icon3':
            iconType = Icon3;
            break;
          case 'icon4':
            iconType = Icon4;
            break;
          case 'icon5':
            iconType = Icon5;
            break;
          case 'icon6':
            iconType = Icon6;
            break;
          case 'icon7':
            iconType = Icon7;
            break;
          default:
            iconType = Icon1;
            break;

        }

        const fun = (userid) => {
          console.log(userid)
          // this.state.userPop = userid;
        }


        return (
          <Marker position={position} icon={iconType} key={key}>
            <Popup onOpen={() => fun(key)}  >
              <div style={{ display: 'flex' }}>
                <div style={{ textAlign: 'center' }} >
                  <span style={{ fontSize: "2.1em" }}>{userName}</span><br />
                  <span style={{ fontSize: "1.2em" }}>{target}<br /> </span>
                  <span>
                    Lorem ipsum et doeri vulti mati <br /> prosbosacoes des ludes.
                  </span><br /><br />
                  <Button color="primary" variant="contained" onClick={() => { let a = key; console.log(a); this.props.toggleDmHandler(a) }}>Donate</Button>
                  <Button color="secondary" variant="contained" onClick={() => { let a = key; console.log(a); this.props.toggleInfoHandler(a) }}>info</Button>
                </div>
              </div>

            </Popup>
          </Marker>
        )
      });
      // console.log(markers)
      return <>{items}</>
    }

    return (
      <Map style={{ height: '100vh', width: "100%" }} center={position} zoom={this.state.zoom} >
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        {/* <MarkerList list={this.state.usersTable} /> */}

        <MyMarkersList markers={this.state.usersTable} />

        <MyMarkersList markers={[
          { key: '0', position: [52.231, 21.1], userName: 'Ławka', target: 'ul. Polska 23', icon: 'icon2' },
          { key: '1', position: [52.23, 21.1], userName: 'Chodnik', target: 'ul. Postępu 32', icon: 'icon3' },
          { key: '2', position: [52.23, 21.05], userName: 'Altanka', target: 'ul. Uliczna 23', icon: 'icon4' },
          { key: '3', position: [52.247, 21.06], userName: 'Przystanek', target: 'Park Saski', icon: 'icon4' },
          { key: '4', position: [52.243, 21.05], userName: 'Winda', target: 'Plac Uni', icon: 'icon6' },
          { key: '5', position: [52.230, 21.10], userName: 'Zieleń', target: 'Skwer Moczydło', icon: 'icon4' },
          { key: '6', position: [52.240, 21.11], userName: 'Park', target: 'ul. Polska 24', icon: 'icon4' },
          { key: '7', position: [52.250, 21.106], userName: 'Fontanna', target: 'ul. Postępu 34', icon: 'icon7' },
          { key: '8', position: [52.220, 21.113], userName: 'Ławka', target: 'ul. Kolska 34', icon: 'icon2' },
        ]
        } />



      </Map >
    )
  }
}


const DmButton = styled.button`
  color:
`
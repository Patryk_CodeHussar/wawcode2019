import React from "react";
// import ReactDOM from "react-dom";
import Sidebar from "react-sidebar";
import MaterialTitlePanel from "./material_title_panel";
import SidebarContent from "./sidebar_content";

import LoginView from '../LoginView/LoginView';

import Header from '../Header/Header';
import Modal from '../Modal/Modal';


const styles = {
  contentHeaderMenuLink: {
    textDecoration: "none",
    color: "white",
    padding: 8
  },
  content: {
    padding: "16px"
  }
};

const mql = window.matchMedia(`(min-width: 800px)`);

class MySideNav extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      docked: mql.matches,
      open: false,
      isOpenModal: true,
    };

    this.mediaQueryChanged = this.mediaQueryChanged.bind(this);
    this.toggleOpen = this.toggleOpen.bind(this);
    this.onSetOpen = this.onSetOpen.bind(this);
  }
  userid = this.props.userid
  componentWillMount() {
    mql.addListener(this.mediaQueryChanged);
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged);
  }

  onSetOpen(open) {
    this.setState({ open });
  }

  mediaQueryChanged() {
    this.setState({
      docked: mql.matches,
      open: false
    });
  }

  toggleOpen(ev) {
    this.setState({ open: !this.state.open });

    if (ev) {
      ev.preventDefault();
    }
  }
  toggleModalHandler = () => {
    console.log('Modal handler')
    this.setState(prevState => ({
      isOpenModal: !prevState.isOpenModal
    }))
  }

  render() {
    const sidebar = <SidebarContent userid={this.userid} />;

    const sidebarProps = {
      sidebar,
      docked: this.state.docked,
      open: this.state.open,
      onSetOpen: this.onSetOpen
    };

    return (<>
      <Header title={'CloudMeets'} toggleSidebar={this.toggleOpen} />
      <Sidebar {...sidebarProps}>
      {/* <LoginView visibility={this.state.isOpenModal} toggleModal={() => this.toggleModalHandler()} /> */}
        {/* <Modal visibility={this.state.isOpenModal} toggleModal={() => this.toggleModalHandler()} /> */}

        {this.props.children}
      </Sidebar>
    </>
    );
  }
}

export default MySideNav 
import React from "react";
import PropTypes from "prop-types";
import MaterialTitlePanel from "./material_title_panel";

import axios from 'axios';
import styled from 'styled-components'

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';



import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'

const styles = {
  sidebar: {
    width: 256,
    height: "80%"
  },
  sidebarLink: {
    display: "block",
    padding: "16px 0px",
    color: "#757575",
    textDecoration: "none"
  },
  divider: {
    margin: "8px 0",
    height: 1,
    backgroundColor: "#757575"
  },
  content: {
    padding: "16px",
    height: "100%",
    backgroundColor: "white"
  }
};

class SidebarContent extends React.Component {
  style = this.props.style
    ? { ...styles.sidebar, ...this.props.style }
    : styles.sidebar;

  state = {
    value: '',
    badania: true,
    recepty: true,
    wizyty: true,
    eZwolnienia: true,
    open: [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,],
    chatMesseges: [
      {
        to: 2,
        listofMessage: [
          {
            from: 2,
            content: "Czesc jestem 1 "
          },
          {
            from: 1,
            content: "Czesc jestem 1 "
          },
          {
            from: 1,
            content: "Czesc jestem 1 po raz drugi"
          },
          {
            from: 2,
            content: "Czesc jestem 2 "
          }
        ],
        from: 1
      },
      {
        to: 1,
        listofMessage: [
          {
            from: 1,
            content: "Czesc jestem 1 po raz drugi"
          }
        ],
        from: 1
      }
    ]
  };

  userid = this.props.userid
  handleClick = (num) => {
    if (
      this.state.open[num] === true
    ) {
      this.setState(state => (state.open[num] = !state.open[num])); //({ open: !state.open }));
      return;
    }
    this.setState(state => (state.open = [false, false, false, false, false,]))
    this.setState(state => (state.open[num] = !state.open[num])); //({ open: !state.open }));
  };


  serverConnection = () => {

    setTimeout(function () {

      var self = this;

      axios.get('http://uranus.wat.edu.pl:7050/test/api/getMessages', { //
        params: {
          id: self.userid
        }
      })
        .then(function (response) {
          // console.log(response.data);




          self.setState({ chatMesseges: response.data })
        })
        .catch(function (error) {
          console.log(error);
        })


      this.serverConnection()
    }.bind(this), 1000);

  }

  componentDidMount() {
    this.serverConnection()
  }

  messageHandler = (event) => {
    this.setState({value: event.target.value});
    console.log(this.state)
  }

  mess = []
  sendMessege = (e) => {
    let self = this;
    this.mess.push(String.fromCharCode(e.keyCode))
    let itbe = this.state.value
    console.log(itbe)

    if (e.keyCode === 13) {

      const config = {
        headers: {
          'Content-Type': 'application/json',
        }
      };
      let data = `{"from":"${self.userid}","content":"${itbe}" }`
      console.log(self.userid);
      let myid = self.userid
      axios.post(`http://uranus.wat.edu.pl:7050/test/api/sendMessage?id=${2}`, data, config)
        .then(function (response) {
          // console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
        this.mess = []
    }
  }

  render() {
    var self = this.state
    const Messeges = this.state.chatMesseges.map((element, index) => (<>

      <ListItem className="list-best white" button onClick={() => this.handleClick(index)}>
        <ListItemText primary={`Chat with ${element.to}`} />
        {this.state.open[index] ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse className="left" in={this.state.open[index]} timeout="auto" unmountOnExit>

        {element.listofMessage.map((inner) => (
          <div>
            {1 == inner.from ?
              <User>{inner.from}</User> :
              <User2>{inner.from}</User2>
            }
            <Messege>{inner.content}</Messege>
          </div>))}


        <SendMsg type="text" value={this.state.value} onKeyDown={this.sendMessege} onChange={this.messageHandler}></SendMsg>
      </Collapse>
    </>)

    )

    return (
      <MaterialTitlePanel title="Chat" style={this.style} >
        <div style={styles.content}>
          <List
            component="nav"
            // subheader={<ListSubheader component="div">Nested List Items</ListSubheader>}
            style={styles.root}
          >

            {Messeges}

          </List>
        </div>
      </MaterialTitlePanel>
    );
  }
};


export default SidebarContent;

const User = styled.div`
display:inline
font-size: 0.8em;



`
const User2 = styled.div`
background-color: #273c75
color:white
display:inline;
margin-bottom: -20px;
margin-left: 93%
font-size: 0.8em;
padding: 2px 10px 2px 10px
border-radius:5px;

`


const Messege = styled.div`
background-color: #273c75
color: #FFF
padding: 5px; 
border-radius:15px


`
const SendMsg = styled.input`
background-color: #00F;
width: 100%;
margin-top:20px;
background: #fff;
border: 1px solid rgba(34,36,38,.15);
color: rgba(0,0,0,.87);
border-radius: .28571429rem

`

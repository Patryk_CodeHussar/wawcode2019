import React, { Component } from 'react';
import { Button, Checkbox, Form } from 'semantic-ui-react';
import axios from 'axios';

import './DmModal.css';

class DmModal extends Component {

    state = {
        isModalOpen: this.props.open,
        message: ''
    }

    userId = this.props.userId;

    handleMessageChange = (event) => {
        this.setState({ message: event.target.value });
    }

    createAccount = (userData) => {
        // console.log(this.props)
        // console.log('Send message')
        // console.log('This props target: ' + this.props.targetID);
        this.sendMessage(this.state.message, this.props.targetID);
        this.props.toggleDmHandler();
    }

    sendMessage = (content, targetId) => {
        // console.log(`Message: ${content}`)
        // console.log(`TargetID: ${targetId}`)
      let self = this;

        const config = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = `{"from":"${self.userId}","content":"${content}" }`
        
        axios.post(`http://uranus.wat.edu.pl:7050/test/api/sendMessage?id=${targetId}`, data, config)
            .then(function (response) {
                // console.log(response);

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {

        const sectionStyle = {
            display: this.props.open ? "block" : "none"
        }


        return (
            <section className='dm-modal' style={sectionStyle}>
                <div className='dm-modal-window'>
                    <div className='dm-modal-window-header'>
                        <span></span>
                        <h3>Wyślij Wiadomość Do:<br /> Jake</h3>
                        <span onClick={this.props.toggleDmHandler}>X</span>
                    </div>

                    <Form>
                        <Form.Field>
                            <label>Wiadomość</label>
                            <input placeholder='Wiadomość' value={this.state.message} onChange={this.handleMessageChange} />
                        </Form.Field>
                        <Button type='submit' onClick={this.createAccount}>Wyślij</Button>
                    </Form>


                </div>
            </section>
        );
    }
}


export default DmModal;
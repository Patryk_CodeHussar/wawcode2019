import React, { Component } from 'react';

import MySideNav from './components/Sidebar/Sidebar';
import Map from './components/Map/Map'
import styled from 'styled-components'
import DmModal from './components/DmModal/DmModal';
import LoginView from './components/LoginView/LoginView';

import { SendMail } from '../components/SendMail'
import { SendMailCustom } from './components/SendMailCustom'


import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import { Link } from 'react-router-dom'


const MapContainer = styled.div`
  height: 90%;
  width: 100%;
  z-index: 1;
  position: absolute;
`

class MapApp extends Component {
  state = {
    userID: 1,
    targetID: null,
    infoID: null
  }

  changeUserIDHandler = (id) => {
    console.log('User created')
    console.log(id)
    this.setState({ userID: id });
  }

  toggleDmHandler = (id) => {
    if (this.state.targetID) {
      this.setState({ targetID: null })
    } else
      this.setState({
        targetID: id
      });

  }

  toogleInfo = (id) => {
    if (this.state.infoID) {
      this.setState({ infoID: null })
    } else
      this.setState({
        infoID: id
      });

  }



  render() {
    return (
      <div>
        {/* <LoginView visibility={!this.state.userID} createUser={this.changeUserIDHandler} /> */}
        {this.state.targetID ? <SendMail open={true} hndlModal={this.toggleDmHandler} userId={this.state.userID} targetID={this.state.targetID} /> : null}
        {this.state.infoID ? <SendMailCustom open={true} hndlModal={this.toogleInfo} userId={this.state.userID} targetID={this.state.infoID} /> : null}
        {/* <Modal visibility={this.state.isOpenModal} toggleModal={() => this.toggleModalHandler()} /> */}
        {/* <MySideNav userid={this.state.userID} > */}
        <MapContainer  >
          <NavBar />
          <Map userid={this.state.userID} toggleInfoHandler={this.toogleInfo} toggleDmHandler={this.toggleDmHandler} />
        </MapContainer>
        {/* </MySideNav> : null */}

      </div>
    );
  }
}

export default MapApp;



const NavBar = () => {

  return (
    <div style={{ overflow: "hidden" }}>
      <AppBar position="static">
        <Toolbar>
          <Linked href="/demo" />Powrót
        </Toolbar>
      </AppBar>
    </div >
  )
}


const Linked = styled.a`
// margin-top:15px;
background-image: url("./resources/mess.png");  
height:40px;
width:40px;
margin-right: 20px;
// position:absolute;
background-size:     cover;                      
background-repeat:   no-repeat;
background-position: center center;              
`
